import React, { Component } from 'react';
import { Navbar, NavbarBrand } from 'reactstrap'
import './App.css'
import { DISHES } from "./shared/dishes";

/* Components*/
import MenuComponent from './component/MenuComponent'
import DishDetail from "./component/DishDetailComponent";

import './App.css';
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dishes: DISHES
    }
  }


  render() {
    return (
      <div className="app">
        <Navbar dark color="primary">
          <div className="container">
            <NavbarBrand href="/">
              Ristorante Con Fusion
          </NavbarBrand>
          </div>
        </Navbar>
        <MenuComponent
          dishes={this.state.dishes}
        />

      </div>
    );
  }
}

export default App;
